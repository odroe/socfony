// Copyright (c) 2021, Odroe Inc. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.
//------------------------------------------------------------------------------
// This is your Prisma schema file,
// learn more about it in the docs: https://pris.ly/d/prisma-schema

generator client {
  provider = "prisma-client-js"
}

datasource db {
  provider = "postgresql"
  url      = env("DATABASE_URL")
}

model User {
  id       String  @id @db.Char(64)
  username String? @unique @db.VarChar(100)
  email    String? @unique @db.VarChar(100)
  phone    String? @unique @db.VarChar(100)
  password String?

  accessTokens AccessToken[]
  comments     Comment[]
  profile      UserProfile?
  moments      Moment[]
  likedMoments UserLikeOnMoment[]

  @@index([username])
  @@index([email])
  @@index([phone])
}

model AccessToken {
  token            String   @id @db.Char(128)
  userId           String   @db.Char(64)
  createdAt        DateTime @default(now()) @db.Timestamp
  expiredAt        DateTime @db.Timestamp
  refreshExpiredAt DateTime @db.Timestamp

  user User @relation(fields: [userId], references: [id])

  @@index([createdAt])
  @@index([expiredAt])
  @@index([refreshExpiredAt])
}

enum OneTimePasswordType {
  EMAIL
  SMS
}

model OneTimePassword {
  type      OneTimePasswordType
  value     String              @db.VarChar(100)
  otp       String              @db.VarChar(100)
  createdAt DateTime            @default(now()) @db.Timestamp
  expiredAt DateTime            @db.Timestamp

  @@id([type, value, otp])
  @@index([createdAt])
  @@index([type])
  @@index([value])
  @@index([otp])
}

enum UserGender {
  UNKNOWN
  WOMAN
  MAN
}

model UserProfile {
  userId   String     @id @db.Char(64)
  avatar   String?    @db.VarChar(255)
  bio      String?    @db.Text
  gender   UserGender @default(UNKNOWN)
  birthday Int?       @db.Integer

  user User @relation(fields: [userId], references: [id])

  @@index([gender])
  @@index([birthday])
}

model Setting {
  type  String
  key   String
  value Json

  @@id([type, key])
}

model Moment {
  id        String   @id @db.Char(64)
  userId    String   @db.Char(64)
  title     String?  @db.VarChar(255)
  content   String?  @db.Text
  media     Json?
  createdAt DateTime @default(now()) @db.Timestamp

  user       User               @relation(fields: [userId], references: [id])
  likedUsers UserLikeOnMoment[]
  comments   Comment[]

  @@index([createdAt])
  @@index([userId])
  @@index([title])
}

model UserLikeOnMoment {
  userId    String   @db.Char(64)
  momentId  String   @db.Char(64)
  createdAt DateTime @default(now()) @db.Timestamp

  user   User   @relation(fields: [userId], references: [id])
  moment Moment @relation(fields: [momentId], references: [id])

  @@id([userId, momentId])
  @@index([createdAt])
}

model Comment {
  id        String   @id @db.Char(64)
  content   String   @db.Text
  userId    String   @db.Char(64)
  createdAt DateTime @default(now()) @db.Timestamp

  // 可选关系
  momentId String? @db.Char(64)

  user   User    @relation(fields: [userId], references: [id])
  moment Moment? @relation(fields: [momentId], references: [id])

  @@index([createdAt])
  @@index([userId])
}
